package pl.vivus;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vivus.domain.Extension;
import pl.vivus.domain.Loan;
import pl.vivus.domain.Person;
import pl.vivus.dto.ApplicationResponseDTO;
import pl.vivus.dto.LoanDTO;
import pl.vivus.dto.LoanMapper;
import pl.vivus.dto.MessageResponseDTO;
import pl.vivus.dto.RequestDTO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class LoanService {

    static final MessageResponseDTO EXTENDED = new MessageResponseDTO("extended");
    static final MessageResponseDTO NOT_FOUND = new MessageResponseDTO("loan not found");
    static final BigDecimal INITIAL_INTEREST = new BigDecimal(15);
    static final double EXTENSION_INTEREST_FACTOR = 1.5;
    static final int EXTENSION_WEEKS = 1;

    @Autowired
    private RiskManager riskManager;
    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private TimeService timeService;

    public ApplicationResponseDTO applyForLoan(RequestDTO application, String userId) {
        boolean risky = riskManager.isRisky(userId);
        if (risky) {
            return ApplicationResponseDTO.REJECTED_MESSAGE;
        }

        Loan loan = createLoan(application, userId);
        return new ApplicationResponseDTO(loan.getId());
    }

    private Loan createLoan(RequestDTO application, String userId) {
        Date issueDate = timeService.getCurrentDate();
        Loan loan = aLoan(application, userId, issueDate);
        return loanRepository.save(loan);
    }

    private Loan aLoan(RequestDTO application, String userId, Date issueDate) {
        Loan loan = new Loan();
        loan.setIssueDate(issueDate);

        Date dueToDate = DateUtils.addDays(issueDate, application.getDays());
        loan.setDueToDate(dueToDate);

        Person person = aPerson(application, userId);
        loan.setPerson(person);

        loan.setAmount(application.getAmount());
        loan.setDays(application.getDays());
        loan.setInterest(INITIAL_INTEREST);

        return loan;
    }

    private Person aPerson(RequestDTO application, String userId) {
        Person person = LoanMapper.INSTANCE.dtoToPerson(application.getPerson());
        person.setUserId(userId);
        return person;
    }

    public MessageResponseDTO extendLoan(Long loanId, String userId) {
        Loan loan = loanRepository.findByIdAndPerson_UserId(loanId, userId);
        if (loan == null) {
            return NOT_FOUND;
        }
        loan.addExtension(anExtension());
        Date extendedDate = DateUtils.addWeeks(loan.getDueToDate(), EXTENSION_WEEKS);
        loan.setDueToDate(new Date(extendedDate.getTime()));
        loan.setInterest(anIncreasedInterest(loan.getInterest()));
        loanRepository.save(loan);
        return EXTENDED;
    }

    private BigDecimal anIncreasedInterest(BigDecimal interest) {
        return interest.multiply(new BigDecimal(EXTENSION_INTEREST_FACTOR));
    }

    private Extension anExtension() {
        Extension extension = new Extension();
        Date currentDate = timeService.getCurrentDate();
        extension.setRequestedDate(currentDate);
        return extension;
    }

    public List<LoanDTO> getHistory(String userId) {

        List<Loan> loans = loanRepository.findByPerson_UserId(userId);
        return LoanMapper.INSTANCE.loansToDTO(loans);
    }
}

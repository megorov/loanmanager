package pl.vivus;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

/**
 * This class evaluate a risk on user for the current moment.
 * The risk considered high if:
 * - request done between 0 and 6AM
 * - user has already obtained 3 loans on the current day
 */
@Service
public class RiskManager {

    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private TimeService timeService;

    /**
     * Evaluates if the specified user at the current moment is considered as low risky to issue the loan.
     *
     * @param userId user identifier
     * @return true if the user is low-risky at the moment, otherwise false.
     */
    public boolean isRisky(String userId) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timeService.getCurrentDate());
        return isBetween0And6(calendar) || has3LoansByCurrentDate(userId, calendar);
    }

    private boolean has3LoansByCurrentDate(String userId, Calendar calendar) {
        Calendar calendarWithoutTime = DateUtils.truncate(calendar, Calendar.DATE);
        Date fromDate = calendarWithoutTime.getTime();
        calendarWithoutTime.add(Calendar.DAY_OF_YEAR, 1);
        Date toDate = calendarWithoutTime.getTime();

        //evaluate by having another loans
        Long loansNumber = loanRepository.countByPerson_UserIdAndIssueDateBetween(userId, fromDate, toDate);
        return loansNumber >= 3;
    }

    private boolean isBetween0And6(Calendar calendar) {
        long hours = DateUtils.getFragmentInHours(calendar, Calendar.DAY_OF_YEAR);
        return hours < 6;
    }
}

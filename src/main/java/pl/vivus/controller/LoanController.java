package pl.vivus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.vivus.LoanService;
import pl.vivus.dto.ApplicationResponseDTO;
import pl.vivus.dto.LoanDTO;
import pl.vivus.dto.MessageResponseDTO;
import pl.vivus.dto.RequestDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Controller class for all rest services provided by application.
 */
@RestController
public class LoanController {

    @Autowired
    private LoanService loanService;

    /**
     * Request for loan application.
     * Retrieves user detail info, response with an issued loan identifier or 'reject' message.
     *
     * @param application user loan application object
     * @param request     http request (injected by spring)
     * @return service response
     */
    @RequestMapping(method = RequestMethod.POST, value = "/apply")
    public ApplicationResponseDTO applyForLoan(@RequestBody RequestDTO application, HttpServletRequest request) {

        return loanService.applyForLoan(application, request.getRemoteAddr());
    }

    /**
     * Service to request to extend the issued loan for 1 week.
     *
     * @param loanId  loan identifier
     * @param request http request (injected by spring)
     * @return success/fail message object
     */
    @RequestMapping(method = RequestMethod.GET, value = "/extend/{loanId}")
    public MessageResponseDTO extendLoan(@PathVariable Long loanId, HttpServletRequest request) {

        return loanService.extendLoan(loanId, request.getRemoteAddr());
    }

    /**
     * Service to request full history for all issued loans including their extension.
     *
     * @param request http request (injected by spring)
     * @return list of all issued loans
     */
    @RequestMapping(method = RequestMethod.GET, value = "/history")
    public List<LoanDTO> getHistory(HttpServletRequest request) {

        return loanService.getHistory(request.getRemoteAddr());
    }
}

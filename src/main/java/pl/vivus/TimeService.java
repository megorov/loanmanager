package pl.vivus;

import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Service that encapsulates retrieving the current time.
 * Useful to mock during tests as in some cases particular dates needed.
 */
@Service
public class TimeService {

    public Date getCurrentDate() {
        return new Date();
    }
}

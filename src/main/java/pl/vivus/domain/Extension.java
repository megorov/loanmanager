package pl.vivus.domain;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Domain class representing an extension for a loan.
 */
@Entity
public class Extension extends IdSupportEntity {

    @Temporal(TemporalType.TIMESTAMP)
    private Date requestedDate;

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }
}

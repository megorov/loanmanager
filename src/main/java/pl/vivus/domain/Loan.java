package pl.vivus.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Domain class representing detail on loan.
 */
@Entity
public class Loan extends IdSupportEntity {

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Person person;

    @Column
    private Integer days;

    @Column(nullable = false, updatable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private BigDecimal interest;

    @Temporal(TemporalType.TIMESTAMP)
    private Date issueDate;

    @Temporal(TemporalType.DATE)
    private Date dueToDate;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Extension> extensions;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getDueToDate() {
        return dueToDate;
    }

    public void setDueToDate(Date dueToDate) {
        this.dueToDate = dueToDate;
    }

    public List<Extension> getExtensions() {
        return extensions;
    }

    public void setExtensions(List<Extension> extensions) {
        this.extensions = extensions;
    }

    public void addExtension(Extension extension) {
        if (extensions == null) {
            setExtensions(new ArrayList<>());
        }
        getExtensions().add(extension);
    }
}

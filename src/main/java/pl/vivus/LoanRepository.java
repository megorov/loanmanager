package pl.vivus;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.vivus.domain.Loan;

import java.util.Date;
import java.util.List;

/**
 * Data base repository. Manipulates with Loan domain objects on the level of relational database.
 */
@Repository
public interface LoanRepository extends CrudRepository<Loan, Long> {

    /**
     * Retrieves all loans owned by a specified user.
     *
     * @param userId user identifier
     * @return list of loans
     */
    List<Loan> findByPerson_UserId(String userId);

    /**
     * Returns a number of loans on specified user in interval between 2 dates.
     *
     * @param userId user identifier
     * @param d1     start interval date
     * @param d2     end interval date
     * @return number of loans
     */
    Long countByPerson_UserIdAndIssueDateBetween(String userId, Date d1, Date d2);

    /**
     * Retrieves a loan by specified ID and owned by the user.
     *
     * @param loanId loan identifier
     * @param userId user identifier
     * @return loan
     */
    Loan findByIdAndPerson_UserId(Long loanId, String userId);
}

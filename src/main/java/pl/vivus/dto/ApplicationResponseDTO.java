package pl.vivus.dto;

/**
 * DTO class to return newly issued loan identifier.
 */
public class ApplicationResponseDTO extends MessageResponseDTO {

    public static final ApplicationResponseDTO REJECTED_MESSAGE = new ApplicationResponseDTO("rejected");
    public static final String OK_MESSAGE = "OK";

    private Long loanId;

    public ApplicationResponseDTO() {
        super();
    }

    public ApplicationResponseDTO(String errorMessage) {
        super(errorMessage);
    }

    public ApplicationResponseDTO(Long loanId) {
        super(OK_MESSAGE);
        this.loanId = loanId;
    }

    public Long getLoanId() {
        return loanId;
    }

    public void setLoanId(Long loanId) {
        this.loanId = loanId;
    }

}

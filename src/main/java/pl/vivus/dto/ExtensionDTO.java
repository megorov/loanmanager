package pl.vivus.dto;

import java.util.Date;

public class ExtensionDTO {

    private Date requestedDate;

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }
}

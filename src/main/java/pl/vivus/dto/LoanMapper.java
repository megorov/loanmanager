package pl.vivus.dto;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.vivus.domain.Extension;
import pl.vivus.domain.Loan;
import pl.vivus.domain.Person;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of this mapper class will be created at build time by a MapStruct plugin.
 */
@Mapper
public interface LoanMapper {
    LoanMapper INSTANCE = Mappers.getMapper(LoanMapper.class);

    LoanDTO loanToDTO(Loan loan);

    PersonDTO personToDTO(Person person);

    ExtensionDTO extensionToDTO(Extension extension);

    Person dtoToPerson(PersonDTO dto);

    default List<LoanDTO> loansToDTO(List<Loan> loans) {
        return loans.stream()
                .map(this::loanToDTO)
                .collect(Collectors.toList());
    }
}
package pl.vivus.dto;

/**
 * DTO to contain a status message for REST services result.
 */
public class MessageResponseDTO {

    protected String result;

    public MessageResponseDTO() {
    }

    public MessageResponseDTO(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}

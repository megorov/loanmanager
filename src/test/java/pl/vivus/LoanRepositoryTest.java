package pl.vivus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.vivus.domain.Extension;
import pl.vivus.domain.Loan;
import pl.vivus.domain.Person;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = App.class)
@Transactional
@Rollback
public class LoanRepositoryTest {

    private static final String USER_ID_1 = "user-id-1";
    private static final String USER_ID_2 = "user-id-2";
    private static final Date REQUESTED_DATE = new Date();
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Autowired
    private LoanRepository repository;

    @Test
    public void shouldFindRequestsByUserId() {
        List<Loan> loans = Arrays.asList(aLoan(USER_ID_1), aLoan(USER_ID_2));
        repository.save(loans);

        List<Loan> requests = repository.findByPerson_UserId(USER_ID_1);
        assertThat(requests, contains(allOf(
                hasProperty("person", hasProperty("userId", is(USER_ID_1))),
                hasProperty("interest", equalTo(new BigDecimal(3))),
                hasProperty("extensions", contains(hasProperty("requestedDate", equalTo(REQUESTED_DATE))))
        )));
    }

    @Test
    public void shouldFindInInterval() throws ParseException {
        saveTestLoans();

        Date d1 = DATE_FORMAT.parse("2017-01-01 00:00");
        Date d2 = DATE_FORMAT.parse("2017-01-02 00:00");
        Long loanNumber = repository.countByPerson_UserIdAndIssueDateBetween(USER_ID_1, d1, d2);
        assertThat(loanNumber, is(1L));
    }

    @Test
    public void shouldNotFindInInterval() throws ParseException {
        saveTestLoans();

        Date d1 = DATE_FORMAT.parse("2017-01-03 00:00");
        Date d2 = DATE_FORMAT.parse("2017-01-04 00:00");
        Long loanNumber = repository.countByPerson_UserIdAndIssueDateBetween(USER_ID_1, d1, d2);
        assertThat(loanNumber, is(0L));
    }

    @Test
    public void shouldFindByLoanIdAndUserId() throws ParseException {
        Loan loan1 = aLoan(USER_ID_1);
        Loan loan2 = aLoan(USER_ID_2);
        repository.save(Arrays.asList(loan1, loan2));

        Loan loan = repository.findByIdAndPerson_UserId(loan1.getId(), loan1.getPerson().getUserId());
        assertThat(loan, samePropertyValuesAs(loan1));
    }

    @Test
    public void shouldNotFindByLoanIdAndUserIdIfNotOwnLoan() throws ParseException {
        Loan loan1 = aLoan(USER_ID_1);
        Loan loan2 = aLoan(USER_ID_2);
        repository.save(Arrays.asList(loan1, loan2));

        Loan loan = repository.findByIdAndPerson_UserId(loan1.getId(), USER_ID_2);
        assertThat(loan, is(nullValue()));
    }

    private Loan aLoan(String userId) {
        Loan loan = new Loan();
        loan.setPerson(aPerson(userId));
        loan.setAmount(new BigDecimal(42));
        loan.setIssueDate(new Date());
        loan.setDueToDate(new Date());
        loan.setInterest(new BigDecimal(3));
        loan.setExtensions(Arrays.asList(anExtension()));
        return loan;
    }

    private Loan aLoan(String userId, Date issueDate) {
        Loan loan = aLoan(userId);
        loan.setIssueDate(issueDate);
        return loan;
    }

    private Extension anExtension() {
        Extension extension = new Extension();
        extension.setRequestedDate(REQUESTED_DATE);
        return extension;
    }

    private Person aPerson(String userId) {
        Person person = new Person();
        person.setUserId(userId);
        person.setCountry("country-1");
        person.setCity("city-1");
        person.setZipCode("123456");
        person.setFirstName("first-name-1");
        person.setSecondName("second-name-1");
        person.setStreetAddress("street-1");
        person.setPhoneNumber("+48 555-555-5555");
        return person;
    }

    private void saveTestLoans() throws ParseException {
        Date issueDate1 = DATE_FORMAT.parse("2017-01-01 13:13");
        Loan loan1 = aLoan(USER_ID_1, issueDate1);

        Date issueDate2 = DATE_FORMAT.parse("2017-01-02 16:16");
        Loan loan2 = aLoan(USER_ID_1, issueDate2);

        List<Loan> loans = Arrays.asList(loan1, loan2);
        repository.save(loans);
    }

}
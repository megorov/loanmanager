package pl.vivus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RiskManagerTest {

    private static final String USER_ID = "user-1";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final String DATE_BEFORE_6 = "2017-02-02 05:12";
    private static final String DATE_AFTER_6 = "2017-02-02 06:12";

    @Mock
    private LoanRepository loanRepository;
    @Mock
    private TimeService timeService;
    @InjectMocks
    private RiskManager riskManager;

    @Test
    public void shouldRejectIfTimeBetween0And6AM() throws ParseException {

        setUpMocks(DATE_BEFORE_6, 0L);

        boolean risky = riskManager.isRisky(USER_ID);
        assertTrue(risky);
    }

    @Test
    public void shouldApproveIfTimeAfter6AM() throws ParseException {

        setUpMocks(DATE_AFTER_6, 0L);

        boolean risky = riskManager.isRisky(USER_ID);
        assertFalse(risky);
    }

    @Test
    public void shouldRejectIfAlready3Loans() throws ParseException {

        setUpMocks(DATE_AFTER_6, 3L);

        boolean risky = riskManager.isRisky(USER_ID);
        assertTrue(risky);
    }

    private void setUpMocks(String dateStr, long loanCount) throws ParseException {
        Date date = DATE_FORMAT.parse(dateStr);
        when(timeService.getCurrentDate()).thenReturn(date);
        when(loanRepository.countByPerson_UserIdAndIssueDateBetween(anyString(), any(Date.class), any(Date.class))).thenReturn(loanCount);
    }
}
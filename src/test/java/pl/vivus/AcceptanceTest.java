package pl.vivus;

import org.apache.commons.lang3.time.DateUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import pl.vivus.dto.ApplicationResponseDTO;
import pl.vivus.dto.LoanDTO;
import pl.vivus.dto.MessageResponseDTO;
import pl.vivus.dto.PersonDTO;
import pl.vivus.dto.RequestDTO;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AcceptanceTest {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final int DAYS = 30;
    private static final int AMOUNT = 3000;
    public static final String FIRST_NAME = "first-name-1";
    public static final String SECOND_NAME = "second-name-1";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private LoanRepository loanRepository;

    @MockBean
    private TimeService timeService;

    @Before
    public void before() throws ParseException {
        Date date = DATE_FORMAT.parse("2017-02-02 10:00");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        given(
                timeService.getCurrentDate())
                .willReturn(date);
        loanRepository.deleteAll();
    }

    @Test
    public void acceptanceTestMax3LoansPerDay() {

        RequestDTO request = aRequest();
        ApplicationResponseDTO response1 = restTemplate.postForObject("/apply", request, ApplicationResponseDTO.class);
        ApplicationResponseDTO response2 = restTemplate.postForObject("/apply", request, ApplicationResponseDTO.class);
        ApplicationResponseDTO response3 = restTemplate.postForObject("/apply", request, ApplicationResponseDTO.class);
        ApplicationResponseDTO response4 = restTemplate.postForObject("/apply", request, ApplicationResponseDTO.class);

        List<ApplicationResponseDTO> successResponses = Arrays.asList(response1, response2, response3);
        //validate successful responses
        assertThat(successResponses, everyItem(allOf(
                hasProperty("result", Matchers.is(ApplicationResponseDTO.OK_MESSAGE)),
                hasProperty("loanId", instanceOf(Long.class)))));

        //validate rejected response
        assertThat(response4, samePropertyValuesAs(ApplicationResponseDTO.REJECTED_MESSAGE));
    }

    @Test
    public void acceptanceTestTimeShouldBeAfter6AM() throws ParseException {
        Date date = DATE_FORMAT.parse("2017-02-02 03:00");
        given(
                timeService.getCurrentDate())
                .willReturn(date);

        ApplicationResponseDTO response = restTemplate.postForObject("/apply", aRequest(), ApplicationResponseDTO.class);

        //validate rejected response
        assertThat(response, samePropertyValuesAs(ApplicationResponseDTO.REJECTED_MESSAGE));
    }

    @Test
    public void acceptanceTestUserShouldBeAbleToExtendLoan() {

        ApplicationResponseDTO applicationResponse = restTemplate.postForObject("/apply", aRequest(), ApplicationResponseDTO.class);

        //validate successful responses
        assertThat(applicationResponse, allOf(
                hasProperty("result", Matchers.is(ApplicationResponseDTO.OK_MESSAGE)),
                hasProperty("loanId", instanceOf(Long.class))));

        MessageResponseDTO extensionResponse = restTemplate.getForObject(
                "/extend/{1}",
                MessageResponseDTO.class,
                applicationResponse.getLoanId());

        //validate rejected response
        assertThat(extensionResponse, samePropertyValuesAs(LoanService.EXTENDED));
    }

    @Test
    public void acceptanceTestUserShouldBeAbleListLoanHistory() {

        ApplicationResponseDTO applicationResponse = restTemplate.postForObject("/apply", aRequest(), ApplicationResponseDTO.class);

        //validate successful responses
        assertThat(applicationResponse, allOf(
                hasProperty("result", Matchers.is(ApplicationResponseDTO.OK_MESSAGE)),
                hasProperty("loanId", instanceOf(Long.class))));

        MessageResponseDTO extensionResponse = restTemplate.getForObject(
                "/extend/{1}",
                MessageResponseDTO.class,
                applicationResponse.getLoanId());

        ResponseEntity<List<LoanDTO>> historyResponse = restTemplate.exchange("/history", HttpMethod.GET, null, new ParameterizedTypeReference<List<LoanDTO>>() {
        });

        //validate rejected response
        assertThat(historyResponse.getBody(), contains(allOf(
                hasProperty("person",
                        allOf(
                                hasProperty("firstName", Matchers.is(FIRST_NAME)),
                                hasProperty("secondName", Matchers.is(SECOND_NAME))
                        )),
                hasProperty("amount", Matchers.is(new BigDecimal(AMOUNT).setScale(2))),
                hasProperty("interest", Matchers.is(anIncreasedInterest())),
                hasProperty("extensions", Matchers.hasSize(1))
        )));
        //validate due-to date
        LoanDTO loanDTO = historyResponse.getBody().get(0);
        assertTrue(DateUtils.isSameDay(loanDTO.getDueToDate(), anExtendedDueToDate()));
    }

    private BigDecimal anIncreasedInterest() {
        return LoanService.INITIAL_INTEREST.multiply(new BigDecimal(LoanService.EXTENSION_INTEREST_FACTOR)).setScale(2);
    }

    private Date anExtendedDueToDate() {
        Date currentDate = timeService.getCurrentDate();
        Date dueToDateWithoutExtension = DateUtils.addDays(currentDate, DAYS);
        return DateUtils.addWeeks(dueToDateWithoutExtension, 1);
    }

    private RequestDTO aRequest() {
        RequestDTO request = new RequestDTO();
        request.setPerson(aPerson());
        request.setAmount(new BigDecimal(AMOUNT));
        request.setDays(DAYS);
        return request;
    }

    private PersonDTO aPerson() {
        PersonDTO person = new PersonDTO();
        person.setCountry("country-1");
        person.setCity("city-1");
        person.setZipCode("123456");
        person.setFirstName(FIRST_NAME);
        person.setSecondName(SECOND_NAME);
        person.setStreetAddress("street-1");
        person.setPhoneNumber("+48 555-555-5555");
        return person;
    }
}

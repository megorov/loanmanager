package pl.vivus;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import pl.vivus.dto.ApplicationResponseDTO;
import pl.vivus.dto.ExtensionDTO;
import pl.vivus.dto.LoanDTO;
import pl.vivus.dto.MessageResponseDTO;
import pl.vivus.dto.RequestDTO;

import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoanControllerTest {

    private static final String LOCAL_HOST = "127.0.0.1";
    private static final long LOAN_ID = 42;
    private static final int DAYS = 14;

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private LoanService loanService;

    @Test
    public void shouldInvokeApply() {
        ArgumentCaptor<RequestDTO> requestCaptor = ArgumentCaptor.forClass(RequestDTO.class);
        ArgumentCaptor<String> userIdCaptor = ArgumentCaptor.forClass(String.class);

        given(
                loanService.applyForLoan(requestCaptor.capture(), userIdCaptor.capture()))
                .willReturn(new ApplicationResponseDTO(LOAN_ID));

        RequestDTO request = aRequest();
        ApplicationResponseDTO response = restTemplate.postForObject("/apply", request, ApplicationResponseDTO.class);

        assertThat(requestCaptor.getValue(), samePropertyValuesAs(request));
        assertThat(userIdCaptor.getValue(), Matchers.is(LOCAL_HOST));
        assertThat(response, allOf(
                hasProperty("result", Matchers.is(ApplicationResponseDTO.OK_MESSAGE)),
                hasProperty("loanId", Matchers.is(LOAN_ID))
        ));
    }

    @Test
    public void shouldInvokeExtend() {
        ArgumentCaptor<String> userIdCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Long> loanIdCaptor = ArgumentCaptor.forClass(Long.class);

        given(
                loanService.extendLoan(loanIdCaptor.capture(), userIdCaptor.capture()))
                .willReturn(new ApplicationResponseDTO(LOAN_ID));

        MessageResponseDTO response = restTemplate.getForObject("/extend/{1}", MessageResponseDTO.class, LOAN_ID);

        assertThat(loanIdCaptor.getValue(), Matchers.is(LOAN_ID));
        assertThat(userIdCaptor.getValue(), Matchers.is(LOCAL_HOST));
        assertThat(response, hasProperty("result", Matchers.is(ApplicationResponseDTO.OK_MESSAGE)));
    }

    @Test
    public void shouldRetrieveLoanHistory() {

        LoanDTO loanDTO1 = aLoanDTO();

        given(
                loanService.getHistory(anyString()))
                .willReturn(
                        asList(loanDTO1)
                );

        ResponseEntity<List<LoanDTO>> result = restTemplate.exchange("/history", HttpMethod.GET, null, new ParameterizedTypeReference<List<LoanDTO>>() {
        });

        assertThat(result.getBody(), contains(
                allOf(
                        hasProperty("id", Matchers.is(loanDTO1.getId())),
                        hasProperty("extensions", Matchers.<List<ExtensionDTO>>contains(
                                hasProperty("requestedDate", Matchers.is(loanDTO1.getExtensions().get(0).getRequestedDate()))))
                )));
    }

    private LoanDTO aLoanDTO() {
        LoanDTO dto = new LoanDTO();
        dto.setId(LOAN_ID);
        dto.setExtensions(singletonList(anExtensionDTO()));
        return dto;
    }

    private ExtensionDTO anExtensionDTO() {
        ExtensionDTO dto = new ExtensionDTO();
        dto.setRequestedDate(new Date());
        return dto;
    }

    private RequestDTO aRequest() {
        RequestDTO request = new RequestDTO();
        request.setDays(DAYS);
        return request;
    }
}
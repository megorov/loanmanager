package pl.vivus;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.vivus.domain.Loan;
import pl.vivus.dto.ApplicationResponseDTO;
import pl.vivus.dto.LoanDTO;
import pl.vivus.dto.LoanMapper;
import pl.vivus.dto.MessageResponseDTO;
import pl.vivus.dto.PersonDTO;
import pl.vivus.dto.RequestDTO;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoanServiceTest {

    private static final String REMOTE_ADDR = "192.168.1.1";
    private static final long LOAN_ID1 = 1L;
    private static final long LOAN_ID2 = 2L;
    private static final int DAYS = 15;
    private static final Date ISSUE_DATE = new Date();
    private static final Date DUE_TO_DATE = new Date();
    private static final BigDecimal INTEREST = new BigDecimal(15);

    @Mock
    private LoanRepository loanRepository;
    @Mock
    private RiskManager riskManager;
    @Mock
    private TimeService timeService;
    @InjectMocks
    private LoanService service;

    @Test
    public void shouldRejectIfRiskHigh() {
        when(riskManager.isRisky(REMOTE_ADDR)).thenReturn(true);
        ApplicationResponseDTO result = service.applyForLoan(aRequest(), REMOTE_ADDR);
        assertEquals(ApplicationResponseDTO.REJECTED_MESSAGE, result);
    }

    @Test
    public void shouldResponseWithLoanRefIfRiskLow() {
        Date date = new Date();

        when(riskManager.isRisky(REMOTE_ADDR)).thenReturn(false);
        when(timeService.getCurrentDate()).thenReturn(date);
        when(loanRepository.save(any(Loan.class))).thenReturn(aLoan());

        ApplicationResponseDTO result = service.applyForLoan(aRequest(), REMOTE_ADDR);
        assertThat(result, allOf(
                hasProperty("result", Matchers.is(ApplicationResponseDTO.OK_MESSAGE)),
                hasProperty("loanId", Matchers.is(LOAN_ID1))
        ));
    }

    @Test
    public void shouldResponseWithSuccessIfLoanIdAndUserOwner() {

        when(loanRepository.findByIdAndPerson_UserId(LOAN_ID1, REMOTE_ADDR)).thenReturn(aLoan());

        MessageResponseDTO messageResponseDTO = service.extendLoan(LOAN_ID1, REMOTE_ADDR);

        assertThat(messageResponseDTO, samePropertyValuesAs(LoanService.EXTENDED));
    }

    @Test
    public void shouldResponseWithFailureIfLoanIdAndUserOwner() {

        when(loanRepository.findByIdAndPerson_UserId(LOAN_ID1, REMOTE_ADDR)).thenReturn(null);

        MessageResponseDTO messageResponseDTO = service.extendLoan(LOAN_ID1, REMOTE_ADDR);

        assertThat(messageResponseDTO, samePropertyValuesAs(LoanService.NOT_FOUND));
    }

    private RequestDTO aRequest() {
        RequestDTO request = new RequestDTO();
        request.setPerson(new PersonDTO());
        request.setDays(DAYS);
        return request;
    }

    private Loan aLoan() {
        return aLoan(LOAN_ID1);
    }

    private Loan aLoan(long id) {
        Loan loan = new Loan();
        loan.setId(id);
        loan.setIssueDate(ISSUE_DATE);
        loan.setDueToDate(DUE_TO_DATE);
        loan.setInterest(INTEREST);
        return loan;
    }

    @Test
    public void shouldResponseWithAListOfLoans() {

        Loan loan1 = aLoan(LOAN_ID1);
        Loan loan2 = aLoan(LOAN_ID2);
        when(loanRepository.findByPerson_UserId(REMOTE_ADDR)).thenReturn(Arrays.asList(loan1, loan2));

        List<LoanDTO> result = service.getHistory(REMOTE_ADDR);

        LoanDTO loanDTO1 = LoanMapper.INSTANCE.loanToDTO(loan1);
        LoanDTO loanDTO2 = LoanMapper.INSTANCE.loanToDTO(loan2);

        assertThat(result, containsInAnyOrder(
                samePropertyValuesAs(loanDTO1),
                samePropertyValuesAs(loanDTO2)
                ));
    }

    @Test
    public void shouldResponseWithAEmptyListOfLoansIfNoLoansOnUser() {

        when(loanRepository.findByPerson_UserId(REMOTE_ADDR)).thenReturn(Collections.emptyList());

        List<LoanDTO> result = service.getHistory(REMOTE_ADDR);

        assertThat(result, empty());
    }
}